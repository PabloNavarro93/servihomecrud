package com.wimso.android_sqlite.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wimso.android_sqlite.R;
import com.wimso.android_sqlite.listener.RecyclerItemClickListener;
import com.wimso.android_sqlite.model.Usuario;
import com.wimso.android_sqlite.widget.LetterTile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wim on 5/1/16.
 */
public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactHolder>{

    private List<Usuario> usuarioList;
    private Context context;

    private RecyclerItemClickListener recyclerItemClickListener;

    public ContactListAdapter(Context context) {
        this.context = context;
        this.usuarioList = new ArrayList<>();
    }

    private void add(Usuario item) {
        usuarioList.add(item);
        notifyItemInserted(usuarioList.size() - 1);
    }

    public void addAll(List<Usuario> usuarioList) {
        for (Usuario usuario : usuarioList) {
            add(usuario);
        }
    }

    public void remove(Usuario item) {
        int position = usuarioList.indexOf(item);
        if (position > -1) {
            usuarioList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public Usuario getItem(int position) {
        return usuarioList.get(position);
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_usuarios, parent, false);

        final ContactHolder contactHolder = new ContactHolder(view);

        contactHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPos = contactHolder.getAdapterPosition();
                if (adapterPos != RecyclerView.NO_POSITION) {
                    if (recyclerItemClickListener != null) {
                        recyclerItemClickListener.onItemClick(adapterPos, contactHolder.itemView);
                    }
                }
            }
        });

        return contactHolder;
    }

    @Override
    public void onBindViewHolder(ContactHolder holder, int position) {
        final Usuario usuario = usuarioList.get(position);

        final Resources res = context.getResources();
        final int tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);

        LetterTile letterTile = new LetterTile(context);

        Bitmap letterBitmap = letterTile.getLetterTile(usuario.getName(),
                String.valueOf(usuario.getId()), tileSize, tileSize);

        holder.thumb.setImageBitmap(letterBitmap);
        holder.rut.setText (usuario.getRut());
        holder.name.setText(usuario.getName());
        holder.lastname.setText(usuario.getLastname());
        holder.phone.setText(usuario.getPhone());
    }

    @Override
    public int getItemCount() {
        return usuarioList.size();
    }

    public void setOnItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }

    static class ContactHolder extends RecyclerView.ViewHolder {

        ImageView thumb;
        TextView rut;
        TextView name;
        TextView lastname;
        TextView phone;

        public ContactHolder(View itemView) {
            super(itemView);

            thumb = (ImageView) itemView.findViewById(R.id.thumb);
            rut = (TextView) itemView.findViewById(R.id.rut);
            name = (TextView) itemView.findViewById(R.id.name);
            lastname = (TextView) itemView.findViewById(R.id.lastname);
            phone = (TextView) itemView.findViewById(R.id.phone);

        }
    }
}
