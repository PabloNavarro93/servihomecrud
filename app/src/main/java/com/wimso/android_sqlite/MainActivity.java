package com.wimso.android_sqlite;

import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wimso.android_sqlite.adapter.ContactListAdapter;
import com.wimso.android_sqlite.db.SQLiteDB;
import com.wimso.android_sqlite.listener.RecyclerItemClickListener;
import com.wimso.android_sqlite.model.Usuario;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerItemClickListener {

    private RecyclerView lvContact;
    private FloatingActionButton btnAdd;

    private ContactListAdapter contactListAdapter;
    private LinearLayoutManager linearLayoutManager;

    private SQLiteDB sqLiteDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("ACT","creada");
        setContentView(R.layout.activity_main);

        lvContact = (RecyclerView) findViewById(R.id.lvContact);
        btnAdd = (FloatingActionButton) findViewById(R.id.add);

        linearLayoutManager = new LinearLayoutManager(this);
        contactListAdapter = new ContactListAdapter(this);
        contactListAdapter.setOnItemClickListener(this);

        lvContact.setLayoutManager(linearLayoutManager);
        lvContact.setAdapter(contactListAdapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetalleUsu.start(MainActivity.this);
            }
        });

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");

        myRef.setValue("Hello, World!");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("ACT","iniciada");
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("ACT","resumida");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("APP","pausada");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("ACT","parada");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("ACT","restablecida");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("ACT","destruida");
    }

    void loadData(){
        sqLiteDB = new SQLiteDB(this);

        List<Usuario> usuarioList = new ArrayList<>();

        Cursor cursor = sqLiteDB.retrieve();
        Usuario usuario;

        if (cursor.moveToFirst()) {
            do {

                usuario = new Usuario();

                usuario.setId(cursor.getInt(0));
                usuario.setRut(cursor.getString(1));
                usuario.setName(cursor.getString(2));
                usuario.setLastname(cursor.getString(3));
                usuario.setPhone(cursor.getString(4));

                usuarioList.add(usuario);
            }while (cursor.moveToNext());
        }

        contactListAdapter.clear();
        contactListAdapter.addAll(usuarioList);
    }

    @Override
    public void onItemClick(int position, View view) {
        DetalleUsu.start(this, contactListAdapter.getItem(position));
    }
}
