package com.wimso.android_sqlite.constant;

/**
 * Created by wim on 4/26/16.
 */
public class UsuarioField {

    public static final String TABLE_NAME = "usuario";
    public static final String COLUMN_ID = "usuario_id";
    public static final String COLUMN_RUT = "usuario_rut";
    public static final String COLUMN_NAME = "usuario_name";
    public static final String COLUMN_LASTNAME = "usuario_lastname";
    public static final String COLUMN_PHONE = "usuario_phone";

}
