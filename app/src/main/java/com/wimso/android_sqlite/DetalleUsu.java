package com.wimso.android_sqlite;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wimso.android_sqlite.db.SQLiteDB;
import com.wimso.android_sqlite.model.Usuario;

/**
 * Created by docotel on 5/2/16.
 */
public class DetalleUsu extends AppCompatActivity implements View.OnClickListener{

    private EditText personRut;
    private EditText personName;
    private EditText personLastname;
    private EditText phone;

    private Button btnAdd, btnEdit, btnDelete;

    private SQLiteDB sqLiteDB;
    private Usuario usuario;

    public static void start(Context context){
        Intent intent = new Intent(context, DetalleUsu.class);
        context.startActivity(intent);
    }

    public static void start(Context context, Usuario usuario){
        Intent intent = new Intent(context, DetalleUsu.class);
        intent.putExtra(DetalleUsu.class.getSimpleName(), usuario);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle_usu);
        Log.i("ACT","creada");

        personRut = (EditText) findViewById(R.id.rutText);
        personName = (EditText) findViewById(R.id.personText);
        personLastname = (EditText) findViewById(R.id.lastnameText);
        phone = (EditText) findViewById(R.id.phoneText);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnDelete = (Button) findViewById(R.id.btnDelete);

        btnAdd.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

        usuario = getIntent().getParcelableExtra(DetalleUsu.class.getSimpleName());
        if(usuario != null){
            btnAdd.setVisibility(View.GONE);

            personRut.setText(usuario.getRut());
            personName.setText(usuario.getName());
            personLastname.setText(usuario.getLastname());
            phone.setText(usuario.getPhone());
        }else{
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
        }

        sqLiteDB = new SQLiteDB(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("ACT","iniciada");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("ACT","resumida");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("ACT","pausada");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("ACT","parada");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("ACT","restablecida");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("ACT","destruida");
    }

    @Override
    public void onClick(View v) {
        if(v == btnAdd){
            usuario = new Usuario();
            usuario.setRut(personRut.getText().toString());
            usuario.setName(personName.getText().toString());
            usuario.setLastname(personLastname.getText().toString());
            usuario.setPhone(phone.getText().toString());
            sqLiteDB.create(usuario);

            Toast.makeText(this, "Agregado Exitosamente", Toast.LENGTH_SHORT).show();
            finish();
        }else if(v == btnEdit){
            usuario.setRut(personRut.getText().toString());
            usuario.setName(personName.getText().toString());
            usuario.setLastname(personLastname.getText().toString());
            usuario.setPhone(phone.getText().toString());
            sqLiteDB.update(usuario);

            Toast.makeText(this, "Editado Exitosamente", Toast.LENGTH_SHORT).show();
            finish();
        }else if(v == btnDelete){
            sqLiteDB.delete(usuario.getId());

            Toast.makeText(this, "Eliminado!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
