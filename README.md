# README #

## Acerca del Proyecto ##
*ServiHome* es un proyecto que busca satisfacer las necesidades de las personas ofreciendoles una plataforma donde puedan **solicitar un servicio general**. Generalmente en ciudades y pueblos los contactos de aquellas personas que realizan estos servicios solo se pasan de boca en boca, por lo que encontrar a alguien que pueda realizar un trabajo de gafitería o contar la leña puede resultar dificultuoso.

Este servicio reunirá a todas estas personas para que puedas encontrarlas facilmente, dandote la posibilidad de poder *realizar un pedido* para que ellos lo vean y se ofrescan. Tu labor será el **detallar el tipo de trabajo** que necesitas y decidir con quien quieres realizar el trato, ya que los usuarios podrán **puntuar el trabajo realizado**.

## Acerca de la APP ##
Para este proyecto de ha creado un **CRUD** para la aplicación movil del registro de los usuarios profesionales que quieran darse a conocer por medio de nosotros, por lo que consistirá en mostrar una **DEMO** de un registro de usuario.

## Conexión con Firebase ##
####*Actualización:*####

Ahora este proyecto cuenta con una conexión a la plataforma de **FireBase de Google.**

Se ha decidido implementar esta herramienta por dos motivos. El primero es debido a la incorporación de un **Login**, o ingreso, a la APP, el cual es mediante correo electrónico. FireBase nos permite realizar una autentificación por correo, por lo que era ideal para nuestro proyecto. Y el segundo motivo de su implementación es a causa de su servicio gratuito, el cual nos da la chance de poder incorporar y probar, totalmente gratis, las ventajas de la autentificación para nuestro proyecto.

## Licencias ##
La aplicación fue hecha en [Android Studios](https://developer.android.com/studio/index.html), con la incorporación y ayuda de:
> 1. [winsonevel](https://github.com/wimsonevel/Android-SQLite), de donde se obtuvo el CRUD de android para usar como base del proyecto
> 2. [Gonzalo Eduardo Pérez Correa](https://www.youtube.com/watch?v=hF_m7B6RyhU), video explicativo para el uso de SQLite y CRUD de android

## Equipo de desarrollo ##
Este proyecto realizado por:
> + Franco López Ranquehue = [franco11](https://bitbucket.org/franco11/)
> + Pablo Navarro Rivas = [PabloNavarro93](https://bitbucket.org/PabloNavarro93/)
> + Cinthya Villarroel Paillacar = [cinthyavvp](https://bitbucket.org/cinthyavvp/)